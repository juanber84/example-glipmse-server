'use strict'

require('@glimpse/glimpse').init()

const express = require('express')
const app = express()
const parameters = require('./parameters.json')

app.listen(parameters.listen, function () {
  console.log(`Example app listening on port ${parameters.listen}!`)
})